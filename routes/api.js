var express = require('express');
var router = express.Router();
const userController = require("../controllers/user");
const categoryController = require("../controllers/category");
const subCategoryController = require("../controllers/subcategory");
const itemController = require("../controllers/item");
const cartController = require("../controllers/cart");
const authHelper = require("../helpers/auth");

router.post("/register", userController.register);
router.post("/login", userController.login);
router.post("/addCategory", categoryController.addCategory);
router.get("/listCategory", categoryController.viewCategorys);
router.get("/viewCategory/:id", categoryController.viewCategory);

router.post("/addSubCategory", subCategoryController.addSubCategory);
router.get("/listSubCategory", subCategoryController.viewSubCategorys);
router.get("/viewSubCategory/:id", subCategoryController.viewSubCategory);

router.post("/addItem", itemController.addItem);
router.get("/listItem", itemController.viewItems);
router.get("/viewItem/:id", itemController.viewItem);

router.post("/addCart",authHelper.userAuthCheck ,cartController.addCart);
router.get("/viewCart",authHelper.userAuthCheck, cartController.viewCart);


module.exports = router;
