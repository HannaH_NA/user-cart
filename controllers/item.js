const mongoose = require("mongoose");
const itemSchema = require("../models/item");
const Item = mongoose.model("Item");

const itemController = {
  addItem: function (req, res) {
    data = req.body;

   
console.log(data.item);

    Item.findOne({name: data.name})
      .then((response) => {
        console.log("test1");

        if (response == null) {
          const item = new Item(data);
          item
            .save()
            .then((result) => {
              console.log("item save to database" + result);
              return res.status(200).json({
                success: true,
                result: result,
              });
            })
            .catch((err) => {
              return res.status(422).json({
                success: false,
                error: err,
              });
            });
        } else {
          return res.status(422).json({
            success: false,
            error: "item already exist",
          });
        }
      })
      .catch((error) => {
        return res.status(422).json({
          success: false,
          error: error,
        });
      });
  },
  viewItem: (req, res) => {
    Item.findById(req.params.id)
    .populate([
        {
          path: "subCategory",
          select: "",
          populate: [
            { path: "category" }
          ]
        }])      .then((result) => {
        console.log("view data", result);
        return res.status(200).json({
          success: true,
          result: result,
        });
      })
      .catch((err) => {
        return res.status(422).json({
          success: false,
          error: err,
        });
      });
  },
  viewItems: (req, res) => {
    Item.find()
    .populate([
        {
          path: "subCategory",
          select: "",
          populate: [
            { path: "category" }
          ]
        }])
      .then((result) => {
        console.log("view data", result);
        return res.status(200).json({
          success: true,
          result: result,
        });
      })
      .catch((err) => {
        return res.status(422).json({
          success: false,
          error: err,
        });
      });
  },

 
};

module.exports = itemController;
