const mongoose = require("mongoose");
const categorySchema = require("../models/category");
const Category = mongoose.model("Category");

const categoryController = {
  addCategory: function (req, res) {
    data = req.body;
    console.log(data.category);

    Category.findOne({ category: data.category })
      .then((response) => {
        console.log("test1");

        if (response == null) {
          const category = new Category(data);
          category
            .save()
            .then((result) => {
              console.log("category save to database" + result);
              return res.status(200).json({
                success: true,
                result: result,
              });
            })
            .catch((err) => {
              return res.status(422).json({
                success: false,
                error: err,
              });
            });
        } else {
          return res.status(422).json({
            success: false,
            error: "category already exist",
          });
        }
      })
      .catch((error) => {
        return res.status(422).json({
          success: false,
          error: error,
        });
      });
  },
  viewCategory: (req, res) => {
    Category.findById(req.params.id)
      .then((result) => {
        console.log("view data", result);
        return res.status(200).json({
          success: true,
          result: result,
        });
      })
      .catch((err) => {
        return res.status(422).json({
          success: false,
          error: err,
        });
      });
  },
  viewCategorys: (req, res) => {
    Category.find()
      .then((result) => {
        console.log("view data", result);
        return res.status(200).json({
          success: true,
          result: result,
        });
      })
      .catch((err) => {
        return res.status(422).json({
          success: false,
          error: err,
        });
      });
  },

  editCategory: (req, res) => {
    Category.findOne({ name: req.body.name }).then((response) => {
      if (response == null) {
        Category.findByIdAndUpdate(req.params.id, { $set: req.body })
          .then((result) => {
            if (result) {
              return res.status(200).json({
                success: true,
                result: "updated ",
              });
            } else {
              return res.status(422).json({
                success: false,
                result: "error",
              });
            }
          })
          .catch((err) => {
            return res.status(422).json({
              success: false,
              result: err,
            });
          });
      } else {
        return res.status(422).json({
          success: false,
          error: "already exist ",
        });
      }
    });
  },
  deleteCategory: (req, res) => {
    Category.findByIdAndRemove(req.params.id)
      .then((response) => {
        if (response) {
          return res.status(200).json({
            success: true,
            result: "Deleted ",
          });
        } else {
          return res.status(422).json({
            success: false,
            result: "category not exist ",
          });
        }
      })
      .catch((error) => {
        return res.status(422).json({
          success: false,
          result: error,
        });
      });
  }
};

module.exports = categoryController;
