const mongoose = require("mongoose");
const cartSchema = require("../models/cart");
const Cart = mongoose.model("Cart");

const cartController = {
    addCart: function (req, res) {
        data = {
            userId: req.headers.currentUser._id,
            quantity: req.body.quantity,
            item: req.body.item
        };
        console.log(req.headers.currentUser._id);
       
        Cart.findOne({  $and: [
            { userId: req.headers.currentUser._id },
            { item: req.body.item },
          ] })
            .then((response) => {
                console.log("test1");

                if (response == null) {
                    const cart = new Cart(data);
                    cart
                        .save()
                        .then((result) => {
                            console.log("save to database" + result);
                            return res.status(200).json({
                                success: true,
                                result: result,
                            });
                        })
                        .catch((err) => {
                            return res.status(422).json({
                                success: false,
                                error: err,
                            });
                        });
                }
                 else {
                     console.log("fgfgfgf",response);
                     var body = {
                        quantity: response.quantity+req.body.quantity,
                     }
                     Cart.findByIdAndUpdate(response._id, { $set: body }).then(
                        (cart) => {
                          console.log("success", cart);
                          return res.status(200).json({
                            success: true,
                            result: cart,
                          });
                        }
                      );
                //     const cart = new Cart(data);
                //     cart
                //         .save()
                //         .then((result) => {
                //             console.log("save to database" + result);
                //             return res.status(200).json({
                //                 success: true,
                //                 result: result,
                //             });
                //         })
                //         .catch((err) => {
                //             return res.status(422).json({
                //                 success: false,
                //                 error: err,
                //             });
                //         });
                }
            })
            .catch((error) => {
                return res.status(422).json({
                    success: false,
                    error: error,
                });
            });
    },

    viewCart: (req, res) => {
        // console.log(req.headers);
        Cart.find({ userId: req.headers.currentUser._id })
            .populate([
                {
                    path: "item",
                    select: "",
                    populate: [
                        {
                            path: "subCategory",
                            select: "",
                            populate: [
                                { path: "category",
                                select:"category"
                             }
                            ]
                        }
                    ]
                }])
            .then((result) => {
                console.log("view data", result);
                return res.status(200).json({
                    success: true,
                    result: result,
                });
            })
            .catch((err) => {
                return res.status(422).json({
                    success: false,
                    error: err,
                });
            });
    },


};

module.exports = cartController;
