const mongoose = require("mongoose");
const subCategorySchema = require("../models/subcategory");
const SubCategory = mongoose.model("SubCategory");

const subCategoryController = {
  addSubCategory: function (req, res) {
    data = req.body;

   
console.log(data.subCategory);

    SubCategory.findOne({subCategory: data.subCategory})
      .then((response) => {
        console.log("test1");

        if (response == null) {
          const subCategory = new SubCategory(data);
          subCategory
            .save()
            .then((result) => {
              console.log("subCategory save to database" + result);
              return res.status(200).json({
                success: true,
                result: result,
              });
            })
            .catch((err) => {
              return res.status(422).json({
                success: false,
                error: err,
              });
            });
        } else {
          return res.status(422).json({
            success: false,
            error: "subCategory already exist",
          });
        }
      })
      .catch((error) => {
        return res.status(422).json({
          success: false,
          error: error,
        });
      });
  },
  viewSubCategory: (req, res) => {
    SubCategory.findById(req.params.id)
    .populate("category")
      .then((result) => {
        console.log("view data", result);
        return res.status(200).json({
          success: true,
          result: result,
        });
      })
      .catch((err) => {
        return res.status(422).json({
          success: false,
          error: err,
        });
      });
  },
  viewSubCategorys: (req, res) => {
    SubCategory.find()
    .populate("category")
      .then((result) => {
        console.log("view data", result);
        return res.status(200).json({
          success: true,
          result: result,
        });
      })
      .catch((err) => {
        return res.status(422).json({
          success: false,
          error: err,
        });
      });
  },

 
};

module.exports = subCategoryController;
