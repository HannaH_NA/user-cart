const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
var subCategorySchema = new Schema({
    subCategory: { type: String, require: true },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Category"
    },
    createdAt: { type: Date, required: false },
    updatedAt: { type: Number, required: false }
});
subCategorySchema.pre("save", function (next) {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    this.updatedAt = now.getTime();
    next();
});
mongoose.model("SubCategory", subCategorySchema);
