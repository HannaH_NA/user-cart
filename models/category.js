const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
var categorySchema = new Schema({
    category: { type: String, require: true },
    createdAt: { type: Date, required: false },
    updatedAt: { type: Date, required: false }
});
categorySchema.pre("save", function (next) {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    this.updatedAt = now.getTime();
    next();
});
mongoose.model("Category", categorySchema);
