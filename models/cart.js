const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
var cartSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    item: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Item"
    },
    quantity:{ type: Number, required: false ,default:1},
    createdAt: { type: Date, required: false },
    updatedAt: { type: Number, required: false }
});
cartSchema.pre("save", function (next) {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    this.updatedAt = now.getTime();
    next();
});
mongoose.model("Cart", cartSchema);
