const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
var itemSchema = new Schema({
    name: { type: String, require: true },
    price: { type: String, require: true },
    subCategory: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "SubCategory"
    },
    createdAt: { type: Date, required: false },
    updatedAt: { type: Number, required: false }
});
itemSchema.pre("save", function (next) {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    this.updatedAt = now.getTime();
    next();
});
mongoose.model("Item", itemSchema);
